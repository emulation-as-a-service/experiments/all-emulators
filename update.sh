#!/bin/sh -x

curl -fL "https://gitlab.com/api/v4/groups/emulation-as-a-service%2Femulators/projects" |
tr -d '\t\n\r' |
grep -E '"web_url" *: *"[^"]+"' -o |
cut -d '"' -f 4 |
tee /dev/stderr |
xargs -t -n 1 git submodule add
